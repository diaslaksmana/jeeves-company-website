//==========================================
//              Sidemenu
//==========================================
document.getElementById("addMenu").addEventListener("click", function () {
  document.body.classList.add("addmenu");
});

document.getElementById("closeMenu").addEventListener("click", function () {
  document.body.classList.remove("addmenu");
});
//==========================================
//              Scrolled
//==========================================
window.addEventListener("scroll", function () {
  var scrolled = window.scrollY > 0;
  document.body.classList.toggle("scrolled", scrolled);
});
//==========================================
//              Owl Carousel
//==========================================
$(document).ready(function () {
  $(".owl-carousel-home").owlCarousel({
    center: true,
    items: 2,
    loop: true,
    margin: 10,
    responsive: {
      600: {
        items: 4,
      },
    },
  });
});
$(document).ready(function () {
  $(".owl-carousel-service").owlCarousel({
    center: true,
    items: 1,
    loop: true,
    nav: true,
    margin: 10,
    responsive: {
      600: {
        items: 3,
      },
    },
  });
});

$(document).ready(function () {
  $(".owl-carousel ").owlCarousel({
    items: 1,
    dots: false,
    loop: true,
    nav: true,
    margin: 10,
  });
});
//==========================================
//             Accordion
//==========================================
document.addEventListener("DOMContentLoaded", function () {
  const accordionItems = document.querySelectorAll(".accordion-item");

  accordionItems.forEach((item) => {
    item
      .querySelector(".accordion-header")
      .addEventListener("click", function () {
        accordionItems.forEach((accItem) => {
          if (accItem !== item && accItem.classList.contains("active")) {
            accItem.classList.remove("active");
          }
        });

        item.classList.toggle("active");
      });
  });
});

// document.addEventListener("DOMContentLoaded", function () {
//   const accordionItems = document.querySelectorAll(".card-service");

//   accordionItems.forEach((item) => {
//     item
//       .querySelector(".btn-show-detail")
//       .addEventListener("click", function () {
//         accordionItems.forEach((accItem) => {
//           if (accItem !== item && accItem.classList.contains("active")) {
//             accItem.classList.remove("active");
//           }
//         });

//         item.classList.toggle("active");
//       });
//   });
// });

function toggleContent(targetId) {
  const content = document.getElementById(targetId);
  if (content.style.display === "none") {
    content.style.display = "block";
  } else {
    content.style.display = "none";
  }
}

// Add event listeners to toggle buttons
const toggleButtons = document.querySelectorAll(".btn-show-detail");
console.log(toggleButtons);
toggleButtons.forEach((button) => {
  button.addEventListener("click", function () {
    const targetId = this.getAttribute("data-target");
    toggleContent(targetId);
  });
});

//==========================================
//            Slider
//==========================================

// window.onload = function () {
//   const sliderWrapper = document.querySelector(".slider-wrapper");
//   const items = document.querySelectorAll(".slider-item");
//   const prevBtn = document.querySelector(".prev");
//   const nextBtn = document.querySelector(".next");
//   const itemWidth = items[0].offsetWidth;
//   const sliderWidth = sliderWrapper.offsetWidth;
//   const itemsToShowDesktop = 3;
//   let currentIndex = 0;

//   function moveSlider(index) {
//     currentIndex = index;
//     const newPosition = -index * itemWidth + (sliderWidth - itemWidth) / 2;
//     sliderWrapper.style.transform = `translateX(${newPosition}px)`;
//     updateActiveClass();
//   }

//   function updateActiveClass() {
//     items.forEach((item, index) => {
//       item.classList.remove("active");
//     });
//     items[currentIndex].classList.add("active");
//   }

//   function prevSlide() {
//     if (currentIndex > 0) {
//       moveSlider(currentIndex - 1);
//     }
//   }

//   function nextSlide() {
//     if (currentIndex < items.length - 1) {
//       moveSlider(currentIndex + 1);
//     }
//   }

//   prevBtn.addEventListener("click", prevSlide);
//   nextBtn.addEventListener("click", nextSlide);

//   // Initially, set the active class for the centered item
//   const initialIndex = Math.floor(itemsToShowDesktop / 2);
//   moveSlider(initialIndex);

//   // Responsive behavior for mobile view
//   function handleResize() {
//     const newSliderWidth = sliderWrapper.offsetWidth;
//     if (newSliderWidth < itemWidth * itemsToShowDesktop) {
//       items.forEach((item) => {
//         item.style.flex = `0 0 ${newSliderWidth}px`;
//       });
//     } else {
//       items.forEach((item) => {
//         item.style.flex = `0 0 ${itemWidth}px`;
//       });
//     }
//     moveSlider(currentIndex);
//   }

//   window.addEventListener("resize", handleResize);
// };
